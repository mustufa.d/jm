<?php

namespace App\Traits;
  
use Illuminate\Http\Request;
use Config;

    trait ValidationTraits{

        public function validateStoresetting()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['store_setting'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }

        public function validateTestimonial()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['testimonial'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }

        public function validateService()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['service'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }

        public function validateCareer()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['career'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }

        public function validateState()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['state'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }

        public function validateAddress()
        {
            $data = [];
                foreach (Config::get('form_layout', 'default')['address'] as $value) {
                    $data[$value['field_name']] = $value['validation'];
                    
                }

        return request()->validate($data);
        }
    }
?>