<?php 

$status = array('0' => 'Enable', '1' => 'Disable');

$data['store_setting'] = [
                    [
                        'field_name' => 'name',
                        'field_label' => 'Name',
                        'field_type' => 'text',
                        'validation' => 'required|min:3|unique:storesettings,name'
                    ],

                    [
                        'field_name' => 'tagline',
                        'field_label' => 'Tagline',
                        'field_type' => 'text',
                        'validation' => 'required|min:3'
                    ],

                    [
                        'field_name' => 'overview',
                        'field_label' => 'Overview',
                        'field_type' => 'ckeditor',
                        'validation' => 'required|min:20|'
                    ],
                    
                    [
                        'field_name' => 'logo1',
                        'field_label' => 'Logo 1',
                        'field_type' => 'file',
                        'validation' => 'required|mimes:png,jpg|min:2048'
                    ],

                    [
                        'field_name' => 'logo2',
                        'field_label' => 'Logo 2',
                        'field_type' => 'file',
                        'validation' => 'required'
                    ],

                    [
                        'field_name' => 'notify_email',
                        'field_label' => 'Notify Email',
                        'field_type' => 'email',
                        'validation' => 'required|email'
                    ],

                    [
                        'field_name' => 'career_email',
                        'field_label' => 'Career Email',
                        'field_type' => 'email',
                        'validation' => 'required|email'
                    ],
                    
                    [
                        'field_name' => 'configuration_email',
                        'field_label' => 'Configuration Email',
                        'field_type' => 'email',
                        'validation' => 'required|email'
                    ],

                    [
                        'field_name' => 'configuration_password',
                        'field_label' => 'Configuration Password',
                        'field_type' => 'password',
                        'validation' => 'required'
                    ],

                    [
                        'field_name' => 'configuration_host',
                        'field_label' => 'Configuration Host',
                        'field_type' => 'text',
                        'validation' => 'required'
                    ],

                    [
                        'field_name' => 'configuration_port',
                        'field_label' => 'Configuration Port',
                        'field_type' => 'text',
                        'validation' => 'required'
                    ],

                    [
                        'field_name' => 'configuration_secure',
                        'field_label' => 'Configuration Secure',
                        'field_type' => 'text',
                        'validation' => 'required|integer'
                    ],

                    [
                        'field_name' => 'meta_title',
                        'field_label' => 'Meta Title',
                        'field_type' => 'text',
                        'validation' => 'required'
                    ],

                    [
                        'field_name' => 'meta_desc',
                        'field_label' => 'Meta Description',
                        'field_type' => 'textarea',
                        'validation' => 'required'
                    ],
                ];

$data['testimonials'] = [
                            [
                                'field_name' => 'name',
                                'field_label' => 'Name',
                                'field_type' => 'text',
                                'validation' => 'required|min:3|unique:testimonials,name'
                            ],

                            [
                                'field_name' => 'title',
                                'field_label' => 'Title',
                                'field_type' => 'text',
                                'validation' => 'required|min:3'
                            ],

                            [
                                'field_name' => 'message',
                                'field_label' => 'Message',
                                'field_type' => 'ckeditor',
                                'validation' => 'required|min:20'
                            ],
                            
                            [
                                'field_name' => 'company',
                                'field_label' => 'Company',
                                'field_type' => 'text',
                                'validation' => 'required|min:3'
                            ],
                            
                            [
                                'field_name' => 'designation',
                                'field_label' => 'Designation',
                                'field_type' => 'text',
                                'validation' => 'required|min:3'
                            ],
                            
                            [
                                'field_name' => 'sort',
                                'field_label' => 'Sorting',
                                'field_type' => 'text',
                                'validation' => 'required|integer'
                            ],

                            [
                                'field_name' => 'status',
                                'field_label' => 'Status',
                                'field_type' => 'select',
                                'options' => $status,
                                'validation' => 'required'
                            ],
                            
                            
                ];



                
$data['career'] = [
                        [
                            'field_name' => 'position',
                            'field_label' => 'Position',
                            'field_type' => 'text',
                            'validation' => 'required|min:3|unique:careers,name'
                        ],
                        
                        [
                            'field_name' => 'location',
                            'field_label' => 'Location',
                            'field_type' => 'text',
                            'validation' => 'required|min:3'
                        ],

                        [
                            'field_name' => 'description',
                            'field_label' => 'Description',
                            'field_type' => 'ckeditor',
                            'validation' => 'required|min:10'
                        ],
                        
                        [
                            'field_name' => 'requirements',
                            'field_label' => 'Requirements',
                            'field_type' => 'ckeditor',
                            'validation' => 'required|min:20'
                        ],
                        
                        [
                            'field_name' => 'sort',
                            'field_label' => 'Sorting',
                            'field_type' => 'text',
                            'validation' => 'required|integer'
                        ],

                        [
                            'field_name' => 'status',
                            'field_label' => 'Status',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'
                        ],
                    
            
];

              
$data['service'] = [
                        [
                            'field_name' => 'title',
                            'field_label' => 'title',
                            'field_type' => 'text',
                            'validation' => 'required|min:3|unique:services,name'
                        ],
                        
                        [
                            'field_name' => 'slug',
                            'field_label' => 'Slug',
                            'field_type' => 'text',
                            'validation' =>  ''
                        ],

                        [
                            'field_name' => 'cover_image',
                            'field_label' => 'Cover Image',
                            'field_type' => 'file',
                            'validation' => 'required'
                        ],
                        
                        [
                            'field_name' => 'short_desc',
                            'field_label' => 'Short Description',
                            'field_type' => 'textarea',
                            'validation' => 'required|min:10'
                        ],
                        
                        [
                            'field_name' => 'service_desc',
                            'field_label' => 'Service Description',
                            'field_type' => 'ckeditor',
                            'validation' => 'required|min:30'
                        ],

                        [
                            'field_name' => 'list_image',
                            'field_label' => 'List Image',
                            'field_type' => 'file',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'meta_title',
                            'field_label' => 'Meta Title',
                            'field_type' => 'text',
                            'validation' => 'required|min:5'
                        ],
    
                        [
                            'field_name' => 'meta_desc',
                            'field_label' => 'Meta Description',
                            'field_type' => 'textarea',
                            'validation' => 'required|min:10'
                        ],
                        
                        [
                            'field_name' => 'sort',
                            'field_label' => 'Sorting',
                            'field_type' => 'text',
                        ],

                        [
                            'field_name' => 'status',
                            'field_label' => 'Status',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'
                        ],
                    ];


 $data['state'] = [
                        [
                            'field_name' => 'name',
                            'field_label' => 'State Name',
                            'field_type' => 'text',
                            'validation' => 'required|min:3|unique:states,name'
                        ],
                        
                        [
                            'field_name' => 'slug',
                            'field_label' => 'Slug',
                            'field_type' => 'text',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'short_desc',
                            'field_label' => 'Short Description',
                            'field_type' => 'textarea',
                            'validation' => 'required|min:10'
                        ],
                        
                        [
                            'field_name' => 'sort',
                            'field_label' => 'Sorting',
                            'field_type' => 'text',
                            'validation' => 'required|integer'
                        ],

                        [
                            'field_name' => 'status',
                            'field_label' => 'Status',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'
                        ],
                    ];

  $data['address'] = [
                        [
                            'field_name' => 'state',
                            'field_label' => 'Choose State',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'

                        ],
                        
                        [
                            'field_name' => 'name',
                            'field_label' => 'Name',
                            'field_type' => 'text',
                            'validation' => 'required|min:3|unique:address,name'
                        ],

                        [
                            'field_name' => 'address',
                            'field_label' => 'Address',
                            'field_type' => 'textarea',
                            'validation' => 'required'
                        ],
                        
                        [
                            'field_name' => 'city',
                            'field_label' => 'City',
                            'field_type' => 'text',
                            'validation' => 'required|min:3'
                        ],
                        
                        [
                            'field_name' => 'direction link',
                            'field_label' => 'Get Direction Link',
                            'field_type' => 'text',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'country',
                            'field_label' => 'Country',
                            'field_type' => 'text',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'phone',
                            'field_label' => 'Phone',
                            'field_type' => 'text',
                            'validation' => 'required|digits:10'
                        ],

                        [
                            'field_name' => 'email',
                            'field_label' => 'Email',
                            'field_type' => 'email',
                            'validation' => 'required|email'
                        ],

                        [
                            'field_name' => 'geo_long',
                            'field_label' => 'Longititude',
                            'field_type' => 'text',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'geo_latitude',
                            'field_label' => 'Latitude',
                            'field_type' => 'text',
                            'validation' => 'required'
                        ],

                        [
                            'field_name' => 'sort',
                            'field_label' => 'Sorting',
                            'field_type' => 'text',
                            'validation' => 'required|digits'
                        ],

                        [
                            'field_name' => 'status',
                            'field_label' => 'Status',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'
                        ],
                    ];


$data['blog_category'] = [
                        [
                            'field_name' => 'name',
                            'field_label' => '  Name',
                            'field_type' => 'text',
                            'validation' => 'required|min:3|unique:blogs,name'
                        ],
                        
                        [
                            'field_name' => 'slug',
                            'field_label' => 'Slug',
                            'field_type' => 'text',
                            'validation' =>  ''
                        ],

                        [
                            'field_name' => 'desc',
                            'field_label' => 'Description',
                            'field_type' => 'textarea',
                            'validation' => 'required|min:10'
                        ],
                        
                        [
                            'field_name' => 'sort',
                            'field_label' => 'Sorting',
                            'field_type' => 'text',
                            'validation' => 'required|digits'
                        ],

                        [
                            'field_name' => 'status',
                            'field_label' => 'Status',
                            'field_type' => 'select',
                            'options' => $status,
                            'validation' => 'required'
                        ],
                    ];

        


        return $data;
?>