<?php 

            $blogs = [
                        [
                            'name' => 'Blog',
                            'link' => 'Admin\BlogController@create'
                        ],

                        [
                            'name' => 'Categories',
                            'link' => 'Admin\BlogController@create'
                        ],

                        [
                            'name' => 'Tag',
                            'link' =>  'Admin\BlogController@create'
                        ],
                    ];
            
            $reach = [
                        [
                            'name' => 'States',
                            'link' => 'Admin\StateController@create'
                        ],

                        [
                            'name' => 'Address',
                            'link' => 'Admin\AddressController@create'
                        ]
                    ];

            $Enquiries = [
                        [
                            'name' => 'General Enquiry',
                            'link' => 'Admin\EnquiryController@create'
                        ]
                    ];
                    
            $webcontent = [
                        [
                            'name' => 'Store Setting',
                            'link' => 'Admin\StoresettingController@create'
                        ],

                        [
                            'name' => 'Testimonials',
                            'link' => 'Admin\TestimonialsController@create'
                        ],

                        [
                            'name' => 'Other Pages',
                            'link' => 'Admin\TestimonialsController@create'
                        ],
                    ];
        return [
                    [
                        'name' => 'Dashboard',
                        'i_class' => 'menu-icon mdi mdi-floor-plan',
                        'link' => '/'
                    ],

                    [
                        'name' => 'Blogs',
                        'i_class' => 'menu-icon mdi mdi-layers-outline',
                        'id' => 'blogs',
                        'link' => '',
                        'li_list' => $blogs
                    ],

                    [
                        'name' => 'Reach',
                        'i_class' => 'menu-icon mdi mdi-layers-outline',
                        'id' => 'reaches',
                        'link' => '',
                        'li_list' => $reach
                    ],

                    [
                        'name' => 'Enquiries',
                        'i_class' => 'menu-icon mdi mdi-layers-outline',
                        'id' => 'enquiry',
                        'link' => '',
                        'li_list' => $Enquiries
                    ],

                    [
                        'name' => 'Servicies',
                        'i_class' => 'menu-icon mdi mdi-bullseye',
                        'link' => 'Admin\ServiceController@create',
                    ],

                    [
                        'name' => 'Career',
                        'i_class' => 'menu-icon mdi mdi-bullseye',
                        'link' => 'Admin\CareerController@create',
                    ],

                    [
                        'name' => 'Web',
                        'i_class' => 'menu-icon mdi mdi-layers-outline',
                        'id' => 'web',
                        'link' => '',
                        'li_list' => $webcontent
                    ],
                    
                ];
?>