<?php

use Illuminate\Database\Seeder;

class DomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = DB::table('states')
                    ->select('name')
                    ->get();

        foreach ($state as $value) {
           
            $doms['state'][strtoLower($value->name)] = $value->name;
                        
        }
        $doms [
            'status' ]= [
                '0' => 'Enable',
                '1' => 'Disable'
            ];
        // $states = [];
       
         foreach ($doms as $group => $value) {
            foreach ($value as $key => $list) {
              
             DB::table('dom_list')->insert([
                 'id' => Str::uuid()->toString(),
                 'group' => $group,
                 'key' => $key,
                 'value' => $list,
                 'created_at'=>now(),
                 'updated_at'=>now()
             ]);

            }
         }
    }
}
