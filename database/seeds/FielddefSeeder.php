<?php

use Illuminate\Database\Seeder;


class FielddefSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Store Setting

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'name',
            'field_label' => 'Name',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required|min:3',
                'placeholder' => 'Name',
                'name' => 'name',
                'id' => 'name'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'tagline',
            'field_label' => 'Tagline',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Tagline',
                'name' => 'tagline',
                'id' => 'tagline'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'overview',
            'field_label' => 'Overview',
            'properties' => json_encode([
                'type' => 'ckeditor',
                'validation' => 'required',
                'placeholder' => 'Overview',
                'name' => 'overview',
                'id' => 'overview'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'logo1',
            'field_label' => 'Logo1',
            'properties' => json_encode([
                'type' => 'file',
                'validation' => 'required',
                'placeholder' => 'Logo1',
                'name' => 'logo1',
                'id' => 'logo1'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'logo2',
            'field_label' => 'Logo2',
            'properties' => json_encode([
                'type' => 'file',
                'validation' => 'required',
                'placeholder' => 'Logo2',
                'name' => 'logo2',
                'id' => 'logo2'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'notify_email',
            'field_label' => 'Notify Email',
            'properties' => json_encode([
                'type' => 'email',
                'validation' => 'required',
                'placeholder' => 'Notify Email',
                'name' => 'notify_email',
                'id' => 'notify_email'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'career_email',
            'field_label' => 'Career Email',
            'properties' => json_encode([
                'type' => 'email',
                'validation' => 'required',
                'placeholder' => 'Career Email',
                'name' => 'career_email',
                'id' => 'career_email'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'configuration_email',
            'field_label' => 'Configuration Email',
            'properties' => json_encode([
                'type' => 'email',
                'validation' => 'required',
                'placeholder' => 'Configuration Email',
                'name' => 'configuration_email',
                'id' => 'configuration_email'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'configuration_password',
            'field_label' => 'Configuration Password',
            'properties' => json_encode([
                'type' => 'password',
                'validation' => 'required',
                'placeholder' => 'Configuration Password',
                'name' => 'configuration_password',
                'id' => 'configuration_password'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'configuration_port',
            'field_label' => 'Configuration Port',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Configuration Port',
                'name' => 'configuration_port',
                'id' => 'configuration_port'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'configuration_secure',
            'field_label' => 'Configuration Secure',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Configuration Secure',
                'name' => 'configuration_secure',
                'id' => 'configuration_secure'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'meta_title',
            'field_label' => 'Meta Title',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Meta Title',
                'name' => 'meta_title',
                'id' => 'meta_title'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Storesetting',
            'field_name' => 'meta_desc',
            'field_label' => 'Meta Description',
            'properties' => json_encode([
                'type' => 'textarea',
                'validation' => 'required',
                'placeholder' => 'Meta Description',
                'name' => 'meta_desc',
                'id' => 'meta_desc'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        
        //Testimonials

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'name',
            'field_label' => 'Name',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required|min:3|unique:testimonials,name',
                'placeholder' => 'Name',
                'name' => 'name',
                'id' => 'name'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'title',
            'field_label' => 'Title',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Title',
                'name' => 'title',
                'id' => 'title'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'message',
            'field_label' => 'Message',
            'properties' => json_encode([
                'type' => 'ckeditor',
                'validation' => 'required',
                'placeholder' => 'Message',
                'name' => 'message',
                'id' => 'message'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'company',
            'field_label' => 'Company',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Company',
                'name' => 'company',
                'id' => 'company'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'designation',
            'field_label' => 'Designation',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Designation',
                'name' => 'designation',
                'id' => 'designation'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'sort',
            'field_label' => 'sort',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'sort',
                'name' => 'sort',
                'id' => 'sort'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Testimonials',
            'field_name' => 'status',
            'field_label' => 'Status',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required',
                'placeholder' => 'Status',
                'name' => 'status',
                'id' => 'status',
                'field_options' => 'status'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        // Service

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'title',
            'field_label' => 'title',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required|min:3|unique:services,title',
                'placeholder' => 'title',
                'name' => 'title',
                'id' => 'title'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'slug',
            'field_label' => 'Slug',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Slug',
                'name' => 'slug',
                'id' => 'slug'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'short_desc',
            'field_label' => 'Short Description',
            'properties' => json_encode([
                'type' => 'textarea',
                'validation' => 'required',
                'placeholder' => 'Short Description',
                'name' => 'short_desc',
                'id' => 'short_desc'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'service_desc',
            'field_label' => 'Service Description',
            'properties' => json_encode([
                'type' => 'ckeditor',
                'validation' => 'required',
                'placeholder' => 'Service Description',
                'name' => 'service_desc',
                'id' => 'service_desc'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'list_image',
            'field_label' => 'List Image',
            'properties' => json_encode([
                'type' => 'file',
                'validation' => 'required',
                'placeholder' => '',
                'name' => 'list_image',
                'id' => 'list_image'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'meta_title',
            'field_label' => 'Meta Title',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Meta Title',
                'name' => 'meta_title',
                'id' => 'meta_title'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'meta_desc',
            'field_label' => 'Meta Description',
            'properties' => json_encode([
                'type' => 'textarea',
                'validation' => 'required',
                'placeholder' => 'Meta Description',
                'name' => 'meta_desc',
                'id' => 'meta_desc'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

          
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'Sort',
            'field_label' => 'sort',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Sort',
                'name' => 'sort',
                'id' => 'sort'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Services',
            'field_name' => 'status',
            'field_label' => 'Status',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required',
                'placeholder' => 'Status',
                'name' => 'status',
                'id' => 'status',
                'field_options' => 'status'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


          // Career

          DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'position',
            'field_label' => 'Position',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required|min:3|unique:careers,position',
                'placeholder' => 'Position',
                'name' => 'position',
                'id' => 'position'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'location',
            'field_label' => 'Location',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Location',
                'name' => 'location',
                'id' => 'location'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'description',
            'field_label' => 'Description',
            'properties' => json_encode([
                'type' => 'ckeditor',
                'validation' => 'required',
                'placeholder' => 'Description',
                'name' => 'description',
                'id' => 'description'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'requirements',
            'field_label' => 'Requirements',
            'properties' => json_encode([
                'type' => 'ckeditor',
                'validation' => 'required',
                'placeholder' => 'Requirements',
                'name' => 'requirements',
                'id' => 'requirements'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
          
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'Sort',
            'field_label' => 'sort',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Sort',
                'name' => 'sort',
                'id' => 'sort'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Career',
            'field_name' => 'status',
            'field_label' => 'Status',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required',
                'placeholder' => 'Status',
                'name' => 'status',
                'id' => 'status',
                'field_options' => 'status'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);


         // State

         DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'State',
            'field_name' => 'name',
            'field_label' => 'State',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required|min:3|unique:states,name',
                'placeholder' => 'State',
                'name' => 'state',
                'id' => 'state'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'State',
            'field_name' => 'slug',
            'field_label' => 'Slug',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Slug',
                'name' => 'slug',
                'id' => 'slug'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'State',
            'field_name' => 'short_desc',
            'field_label' => 'Short Description',
            'properties' => json_encode([
                'type' => 'textarea',
                'validation' => 'required',
                'placeholder' => 'Short Description',
                'name' => 'short_desc',
                'id' => 'short_desc'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
          
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'State',
            'field_name' => 'Sort',
            'field_label' => 'sort',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Sort',
                'name' => 'sort',
                'id' => 'sort'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'State',
            'field_name' => 'status',
            'field_label' => 'Status',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required',
                'placeholder' => 'Status',
                'name' => 'status',
                'id' => 'status',
                'field_options' => 'status'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);



        // Address

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'state',
            'field_label' => 'State',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required|min:3|unique:addresses,state',
                'placeholder' => 'State',
                'name' => 'state',
                'id' => 'state',
                'field_options' => 'state'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'name',
            'field_label' => 'Name',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Name',
                'name' => 'name',
                'id' => 'name'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'address',
            'field_label' => 'Address',
            'properties' => json_encode([
                'type' => 'textarea',
                'validation' => 'required',
                'placeholder' => 'Address',
                'name' => 'address',
                'id' => 'address'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        
        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'city',
            'field_label' => 'City',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'City',
                'name' => 'city',
                'id' => 'city'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'direction link',
            'field_label' => 'Get Direction Link',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Get Direction Link',
                'name' => 'direction link',
                'id' => 'direction link'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'country',
            'field_label' => 'Country',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Country',
                'name' => 'country',
                'id' => 'country'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);      

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'phone',
            'field_label' => 'Phone',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Phone',
                'name' => 'phone',
                'id' => 'phone'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'email',
            'field_label' => 'Email',
            'properties' => json_encode([
                'type' => 'email',
                'validation' => 'required',
                'placeholder' => 'Email',
                'name' => 'email',
                'id' => 'email'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'geo_long',
            'field_label' => 'Logititude',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Logititude',
                'name' => 'geo_long',
                'id' => 'geo_long'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'geo_lat',
            'field_label' => 'Latitude',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Latitude',
                'name' => 'geo_lat',
                'id' => 'geo_lat'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'Sort',
            'field_label' => 'sort',
            'properties' => json_encode([
                'type' => 'text',
                'validation' => 'required',
                'placeholder' => 'Sort',
                'name' => 'sort',
                'id' => 'sort'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('field_defs')->insert([
            'id' => Str::uuid()->toString(),
            'module_name' => 'Address',
            'field_name' => 'status',
            'field_label' => 'Status',
            'properties' => json_encode([
                'type' => 'select',
                'validation' => 'required',
                'placeholder' => 'Status',
                'name' => 'status',
                'id' => 'status',
                'field_options' => 'status'
            ]),
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }
}
