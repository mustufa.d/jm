@extends('layout')

@section('content')

<div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">State</h4>
        <form class="form-sample">
          {{-- <p class="card-description">
            Personal info
          </p> --}}
          <div class="row">
            {!! Form::model(null, ['method' => 'PUT']) !!}
            {{-- @dd(Config::get('form_layout', 'default')['store_setting']) --}}
            @foreach (Config::get('form_layout', 'default')['state']; as $item)
            @include('Form.form')
            @endforeach
          </div>
          {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
          {!! Form::close() !!}
        </form>
      </div>
    </div>
  </div>
    
@endsection