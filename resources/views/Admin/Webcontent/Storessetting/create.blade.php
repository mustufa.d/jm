@extends('layout')

@section('content')

<div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Store Setting</h4>
        {{-- <form class="form-sample"> --}}
          {{-- <p class="card-description">
            Personal info
          </p> --}}
          {!! Form::open(array( 'class' => 'form-sample','action'=> 'Admin\StoresettingController@store','method'=>'post','files'=> true)) !!}
          <div class="row">
            {!! Form::token() !!}
            {{-- @foreach (Config::get('form_layout', 'default')['store_setting']; as $item) --}}
              
              @include('Form.form')

            {{-- @endforeach --}}
            
          </div>
          {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
          {!! Form::close() !!}
        {{-- </form> --}}
      </div>
    </div>
  </div>
    
@endsection