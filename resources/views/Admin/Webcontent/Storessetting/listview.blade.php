@extends('layout')

@section('content')
<div class="table-responsive">
<table id="datatableStoresetting" class="table table-bordered text-nowrap key-buttons">
    <thead>
        <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Age</th>
            <th>Start date</th>
            <th>wfsedc</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tiger Nixon</td>
            <td class="table_email">mustufa.d@enjayworld.com</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>2011/04/25</td>
            <td>vcxv</td>
        </tr>
        <tr>
            <td>Tiger Nixon</td>
            <td class="table_email">mustufadarugar786@gmail.com</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>2011/04/25</td>
            <td>vcxv</td>
        </tr>
        <tr>
            <td>Tiger Nixon</td>
            <td class="table_email">mustufadarugar12@gmail.com</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>2011/04/25</td>
            <td>vcxv</td>
        </tr>
       
    </tbody>
</table>
</div>
@endsection