
@php
    $module = DB::table('field_defs')
                ->where('module_name',$module)
                ->get();

  
@endphp


@foreach ($module as $field)
    
@if (json_decode($field->properties)->type == 'textarea')
<div class="col-md-12">
  <div class="form-group row">
    @include('field_labels.label', ['text'=>$field->field_label, 'for' => $field->field_label])  
    <div class="col-md-12">
      @include('fields.'.json_decode($field->properties)->type , ['name' => json_decode($field->properties)->name,'value'=> $data->name ?? '','attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'ckeditor', 'data-name'=>json_decode($field->properties)->name)])
    </div>
  </div>
</div>

@elseif (json_decode($field->properties)->type == 'ckeditor')
   <div class="col-md-12">
      <div class="form-group row">
            @include('field_labels.label', ['text'=>$field->field_label, 'for' => $field->field_label])  
               <div class="col-md-12">
                      @include('fields.'.json_decode($field->properties)->type , ['name' => json_decode($field->properties)->name,'value'=> $data->name ?? '','attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'ckeditor', 'data-name'=>json_decode($field->properties)->name)])
                </div>
            </div>
      </div>
    
@elseif (json_decode($field->properties)->type == 'select')
<div class="col-md-6">
  <div class="form-group row">
    @include('field_labels.label', ['text'=>$field->field_label, 'for' => $field->field_label])  
    <div class="col-sm-9">        
                @include('fields.'.json_decode($field->properties)->type , ['name' => json_decode($field->properties)->name, 'list'=>domFetch(json_decode($field->properties)->field_options), 'select'=>$data->education ?? '', 'attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'form-control', 'data-name'=>json_decode($field->properties)->name)])
    </div>
  </div>
</div>
@elseif (json_decode($field->properties)->type == 'multiselect')
                {{-- @dd($item['options']) --}}
      @if (isset($data))
                       
        @include('fields.multiselect' , ['name' => json_decode($field->properties)->name ,'list'=>$item['options'],'select'=>$data->hobbies,  'attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'form-control', 'data-name'=>json_decode($field->properties)->name)])   
        @include('fields.hidden' , ['name' => json_decode($field->properties)->name, 'value'=> '', 'attributes'=> array('id'=>json_decode($field->properties)->id)])   
                   
      @else
        @include('fields.multiselect' , ['name' => json_decode($field->properties)->name ,'list'=> $item['options'], 'select'=>'' ,  'attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'form-control', 'data-name'=>json_decode($field->properties)->name)])       
                
        @include('fields.hidden' , ['name' => $item['field_hidden'], 'value'=> '','attributes'=> array('id'=>$item['field_hidden'])])  
      @endif
      @elseif (json_decode($field->properties)->type == 'file')
      <div class="col-md-6">
        <div class="form-group row">
          @include('field_labels.label', ['text'=>$field->field_label, 'for' => $field->field_label])  
          <div class="col-sm-9">
            @include('fields.'.json_decode($field->properties)->type , ['name' => json_decode($field->properties)->name,'value'=> $data->name ?? '','attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'btn btn-outline-primary btn-icon-text', 'data-name'=>json_decode($field->properties)->name)])
            <!-- images card -->
            <div class="card {{json_decode($field->properties)->name}}">
              <button type="button" class="close cls-img btn btn-sm w-25 btn-outline-danger float-right" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            <img class="card-img-top {{json_decode($field->properties)->name}}" src="..." alt="Card image cap">
            </div>
            <!-- image card end -->
            @error(json_decode($field->properties)->name)
              <p class="text text-danger">{{$message}}</p>            
            @enderror
          </div>                  
         </div>
      </div>
      
      @else
          <div class="col-md-6">
            <div class="form-group row">
              @include('field_labels.label', ['text'=>$field->field_label, 'for' => $field->field_label])  
              <div class="col-sm-9">
                @include('fields.'.json_decode($field->properties)->type , ['name' => json_decode($field->properties)->name,'value'=> $data->name ?? '','attributes'=> array('id'=>json_decode($field->properties)->id, 'class'=>'form-control', 'data-name'=>json_decode($field->properties)->name)])
              </div>
            </div>
          </div>
  @endif
@endforeach