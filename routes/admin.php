<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('store-setting', 'StoresettingController');

Route::resource('testimonial', 'TestimonialsController');

Route::resource('state', 'StateController');

Route::resource('address', 'AddressController');

Route::resource('service', 'ServiceController');

Route::resource('career', 'CareerController');

Route::resource('blog', 'BlogController');

Route::resource('enquiry', 'EnquiryController');